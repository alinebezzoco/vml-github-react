# Github React
Página desenvolvida em React usando a API pública do Github para consumo de informação. 

## Sobre o projeto
O Github React consiste em exibir uma lista de repositórios da Globo.com e ao clicar em um repositório é exibido o histórico dos commits contendo informações como autor, data, mensagem e id do commit. 

## Como rodar o projeto 
1. Faça um clone desse repositório na sua máquina local usando o comando `git clone <uri do repositório>`; 
2. Abra o diretório do projeto dentro do terminal usando `cd <nome do projeto>`; 
3. Dê `npm install` para instalar as dependências; 
4. Finalizado a instalação basta dar o comando `npm start` para inicializar o projeto através do `localhost:3000`.

## Componentes 
* Sidebar 
* Header 

## Páginas 
* Home 
* Main 
* Repo Details

## Tecnologias utilizadas 
* React 
* HTML5 
* CSS3 
* Vanilla JS (Javascript) 
* SASS 

## Plugins 
* React Router v4 
* React Burger Menu 
* Node SASS
* Axios 

## Requisições mínimas necessárias para rodar o projeto 
* Node v8+
* NPM v5+