import React from 'react';
import Sidebar from './Sidebar';

export default class Header extends React.Component {

    render() {
        return (
            <header className="header">
               <Sidebar />
            </header>
        );
    }
}
