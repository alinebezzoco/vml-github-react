import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import loading from '../loading.svg';

export default class Sidebar extends Component {

    state = {
        repos: [],
        isLoading: true,
        errors: null
    }

    getRepos() {
      let username = 'globocom'; 
      
      axios
        .get(`https://api.github.com/users/${username}/repos?&per_page=100&page=1`)
        .then(response =>
          response.data.map(repo => ({
            id: `${repo.id}`,
            name: `${repo.name}`,
            description: `${repo.description}`,
            stargazers_count: `${repo.stargazers_count}`,
            forks_count: `${repo.forks_count}`
          }
          ))
        )
        .then(repos => {
            repos.sort(function(a, b){
                return b.stargazers_count - a.stargazers_count;
            });
            this.setState({
                repos,
                isLoading: false
          });
        })
        .catch(error => this.setState({ error, isLoading: false }));
    }
  
    componentDidMount() {
      this.getRepos();
    }
  
    render() {
      const { isLoading, repos } = this.state;
      return (
        <div className="sidebar">
            <div className="content">
                <h4><Link to="/">Globo.com Github</Link></h4>
                <h2>Lista de Repositórios</h2>
                <ul>
                    {!isLoading ? (
                        repos.map(repo => {
                            const { id, name, description, stargazers_count, forks_count } = repo; 
                            return (
                                <Link to={`/${name}`} key={id}>
                                    <li className={id}>
                                        <p><strong>{name}</strong></p>
                                        <p>{description}</p>
                                        <p><i className="fa fa-star" aria-hidden="true"></i> {stargazers_count} | <i className="fa fa-code-branch" aria-hidden="true"></i> {forks_count}</p>
                                        <hr />
                                    </li>
                                </Link>
                            );
                        })
                    ) : (
                    <img src={loading} className="loader"  alt="loading" />
                    )}
                </ul>
            </div>
        </div>
      );
    }
}