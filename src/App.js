import React, {Component} from 'react';
import Header from './components/Header';
import Main from './pages/Main';
import { reveal as Menu } from "react-burger-menu";

class App extends Component {

  render(){
    return(
      <div className="app">
        <Menu isOpen={ true } pageWrapId={ "page-wrap" } noOverlay>
          <Header />
        </Menu>
        <div id="page-wrap" className="container">
          <Main/>
          {this.props.children}
        </div>
      </div>
    );
  }
}
export default App;
