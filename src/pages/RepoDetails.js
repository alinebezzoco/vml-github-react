import React, { Component } from 'react';
import axios from 'axios';
import loading from '../loading.svg';

export default class RepoDetails extends Component {

  state = {
    commits: [],
    repo: {},
    errors: null,
    isLoading: true
  }
  getCommits = this.getCommits.bind(this);
  
  getCommits(){
    let username = 'globocom'; 
    let page = sessionStorage.getItem('page');
    let selectedRepo = window.location.pathname;
    axios
    .get(`https://api.github.com/repos/${username}${selectedRepo}/commits?per_page=20&page=${page}`)
    .then(response =>
      response.data.map(data => ({
        message: `${data.commit.message}`,
        author: `${data.commit.author.name}`,
        date: `${data.commit.author.date}`,
        sha: `${data.sha}`,
        html_url: `${data.html_url}`
      }
      ))
      )
      .then(data => {
        if(data.length > 20){
          document.querySelector(".load-more").style.display = "none";
        } 
        for(let i = 0; i < data.length; i++) {
          let commits = this.state.commits;
          commits.push(data[i]);
        }
        let commits = this.state.commits;
        this.setState({commits, isLoading: false});
    })
    .catch(error => this.setState({ error, isLoading: false }));
    let next = parseInt(page) + 1;
    sessionStorage.setItem('page', next);
  }

  getRepos() {
    let username = 'globocom'; 
    let selectedRepo = window.location.pathname;
    axios
      .get(`https://api.github.com/repos/${username}${selectedRepo}`)
      .then(response =>
        this.setState({repo: response, isLoading: false })
      )
      .catch(error => this.setState({ error, isLoading: false }));
      sessionStorage.setItem('page', 1);
  }

  componentDidMount(){
    this.getRepos();
    this.getCommits();
  }

  componentDidUpdate(){
    return true;
  }

  componentWillReceiveProps(){
    this.getRepos();
    this.getCommits();

    this.setState(() => {
      return ({commits:[]});
    });
    
  }

  render(){
    const { commits, isLoading } = this.state;
    const selectedRepo = window.location.pathname;
    const repoName = selectedRepo.replace("/", "");
    
    return(
      <div className="repoDetails">
        <div className="content">
          <h2>Commits do repositório {repoName}</h2>
          <table>
            <thead>
              <tr>
                <th>Author</th>
                <th>Commit</th>
                <th>Message</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {!isLoading ? (
                commits.map(data => {
                  const { message, author, sha, html_url } = data;
                  const commitId = data.sha.slice(-7);
                  const commitDate = data.date.slice(0,10);
                  return (
                    <tr key={sha}>
                      <td>{author}</td>
                      <td><a href={html_url} key={commitId}>{commitId}</a></td>
                      <td>{message}</td>
                      <td>{commitDate}</td>
                    </tr>
                  );
                })               
                ) : (
                  <img src={loading} className="loader" alt="loading" />
                )}
            </tbody>
          </table>
          <div className="load-more">
            <button onClick={this.getCommits}>Carregar mais commits</button>  
          </div>
        </div>
      </div>
    );
  }
}
