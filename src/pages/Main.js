import React from 'react'
import { Switch, Route } from 'react-router-dom';
import RepoDetails from './RepoDetails';
import Home from './Home';

const Main = () => (
  <main>
    <Switch>
        <Route exact path="/" component={ Home } />
        <Route path="/:repo" component={ RepoDetails } />
    </Switch>
  </main>
)

export default Main
