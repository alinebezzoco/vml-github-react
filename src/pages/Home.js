import React from 'react';

const Home = () => (
    <div className="home">
        <div className="content">
            <i className="fab fa-github"></i>
            <h1>Olar =)</h1>
            <p>Selecione um repositório e visualize os commits de cada um deles</p>
        </div>
    </div>
)

export default Home
